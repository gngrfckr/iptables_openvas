# -*- coding: utf-8 -*-
import sys
import os
import pandas as pd
import math
import glob
import subprocess
from flask import Flask, jsonify, abort, make_response, request, session, render_template, redirect, url_for
from werkzeug.utils import secure_filename
import netifaces as ni
from functools import wraps
from werkzeug.contrib.fixers import ProxyFix


UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = set(['csv'])
ACCEPTED_BY_DEFAULT = [22, 443, 80]
PATH = "./whitelist" 
REPORT_CSV = './uploads/report.csv'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
config = {}

list_of_files = glob.glob(UPLOAD_FOLDER + '/*')
latest_file = max(list_of_files, key=os.path.getctime)




def iface_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if  config.get('interface') is None:
                return make_response(jsonify({'error': 'interface is not defined'}), 500)
        return func(*args, **kwargs)
    return decorated_view


def rule_exist(rule):
        cmd = subprocess.Popen('iptables -S', shell=True, stdout=subprocess.PIPE)
        for line in cmd.stdout:
                if  rule[11:] in str(line)[4:]:
                        print('Rule exists')
                        return False
        return True
                
def get_rules():
        iptables_out = subprocess.Popen('./sbin/iptables -S', shell=True, stdout=subprocess.PIPE)
        rules = [rule for rule in iptables_out.stdout]   
        return rules             


def get_ports(path_csv):
    """Парсит отчет OpenVas возвращает список портов"""
    df = pd.read_csv(path_csv)
    ports = list(set(df.Port.tolist()))
    result = []
    for port in ports:
        if not math.isnan(port):
            result.append(int(port))
    return result


def get_white_list(path):
    """Получает список разрешенных адресов"""
    with open(path, 'r') as white_list:
        return "".join(white_list).split("\n")


def accept(iface, ports):
    """Открывает доступ для наших ip адресов из белого списка"""
    whitelist = get_white_list(PATH)
    for port in ports:
        if port in ACCEPTED_BY_DEFAULT:
                continue
        for ip in whitelist:
                rule = './sbin/iptables -I DOCKER -i ' + iface + ' -s ' + str(ip) + ' -p tcp -m conntrack --ctorigdstport ' + str(port) + ' -j ACCEPT'
                if rule_exist(rule):
                        os.system(rule)


def deny(iface, ports):
    """Закрывает порт iptables в табличке Docker"""
    for port in ports:
        rule = './sbin/iptables -I DOCKER -i ' + iface + ' -p tcp -m conntrack --ctorigdstport ' + str(port) + ' -j DROP'
        if port in ACCEPTED_BY_DEFAULT:
            continue  
        if rule_exist(rule):   
                os.system(rule) 



@app.route('/all', methods=['GET'])
def all_data():
        return make_response(jsonify(get_rules()), 200)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return make_response(jsonify(get_rules()), 200)


@app.route('/run', methods=['POST'])
@iface_required
def run():
      deny(config.get('interface'), get_ports(latest_file))
      accept(config.get('interface'), get_ports(latest_file))
      return make_response(jsonify({'success':'True'}), 200)
        
@app.route('/interface', methods=['POST'])
def set_interface():
        config['interface'] = request.json['interface']
        return make_response(jsonify(request.json['interface']), 200)

@app.route('/interface', methods=['GET'])
@iface_required
def get_interface():
        return make_response(jsonify(config.get('interface')), 200)


@app.route('/interface/all', methods=['GET'])
def get_interfaces():
        return make_response(jsonify(ni.interfaces()), 200)

app.wsgi_app = ProxyFix(app.wsgi_app)
if __name__ == "__main__":
      app.run()

    
